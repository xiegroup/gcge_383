CPP      = mpicxx
CC       = mpicc

LIBBLASLAPACK = 


PETSCFLAGS = -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fstack-protector -fvisibility=hidden -g3  
PETSCINC   = -I${PETSC_DIR}/include -I${PETSC_DIR}/include/petsc/private  
LIBPETSC   = -L${PETSC_DIR}/lib -Wl,-rpath,${PETSC_DIR}/lib -lpetsc \
            -Wl,-rpath,/soft/apps/mpich/3.4-gnu/lib -L/soft/apps/mpich/3.4-gnu/lib -Wl,-rpath,/usr/lib/gcc/x86_64-redhat-linux/4.8.5 \
            -L/usr/lib/gcc/x86_64-redhat-linux/4.8.5 \
            -lsuperlu -lcmumps -ldmumps -lsmumps -lzmumps -lmumps_common -lpord \
            -lscalapack -lsuperlu_dist -lflapack -lfblas -lparmetis -lmetis -lX11 \
            -lpthread -lm -lmpifort -lgfortran -lquadmath -lmpicxx -lstdc++ \
            -Wl,-rpath,/soft/apps/mpich/3.4-gnu/lib -L/soft/apps/mpich/3.4-gnu/lib \
            -Wl,-rpath,/usr/lib/gcc/x86_64-redhat-linux/4.8.5 -L/usr/lib/gcc/x86_64-redhat-linux/4.8.5 \
            -ldl -Wl,-rpath,/soft/apps/mpich/3.4-gnu/lib -lmpi -lgcc_s
  
SLEPCFLAGS = -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fstack-protector -fvisibility=hidden -g3
SLEPCINC   = -I${SLEPC_DIR}/include -I${PETSC_DIR}/include 
LIBSLEPC   = -Wl,-rpath,${SLEPC_DIR}/lib -L${SLEPC_DIR}/lib -lslepc -Wl,-rpath,${PETSC_DIR}/lib -L${PETSC_DIR}/lib  
