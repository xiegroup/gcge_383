#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <memory.h>
#include <time.h>
#include <float.h>
#include "ops.h"
#include "ops_eig_sol_gcg.h"
#include "app_slepc.h"
#if 1
int TestEigenSolverGCG   (void *A, void *B, int flag, int argc, char *argv[], struct OPS_ *ops, double shift);
int TestAppSLEPC (int argc, char *argv[]);

static char help[] = "Test App of SLEPC.\n";
int TestEigenSolverGCG(void *A, void *B, int flag, int argc, char *argv[], struct OPS_ *ops, double shift) 
{

	int nevConv  = 10; //number of required eigenpairs 
	double gapMin = 1e-5, abs_tol, rel_tol;
	int nevGiven = 0, block_size = nevConv/2, nevMax = 2*nevConv, multiMax = 1, nevInit = nevMax;
	abs_tol = 1e-7; //abs tolerance
	rel_tol = 1e-9; //rel tolerance
	//to choose the method for linear solver
	char compW_method[8] = "bcg"; //mumps

	if (nevConv > 300)
	{
		block_size = nevConv / 10;
		nevInit = 5*block_size; 
		nevMax =  nevConv + nevInit;
	}

	ops->GetOptionFromCommandLine("-nevConv"  ,'i',&nevConv   ,argc,argv,ops);
	ops->GetOptionFromCommandLine("-nevMax"   ,'i',&nevMax    ,argc,argv,ops);
	ops->GetOptionFromCommandLine("-blockSize",'i',&block_size,argc,argv,ops);
	ops->GetOptionFromCommandLine("-nevInit"  ,'i',&nevInit   ,argc,argv,ops);
	nevInit = nevInit<nevMax?nevInit:nevMax;
	int max_iter_gcg = 10000; 
	double tol_gcg[2] = {1e-7,1e-9};
	tol_gcg[0] = abs_tol;
	tol_gcg[1] = rel_tol;

	double *eval; void **evec;
	eval = malloc(nevMax*sizeof(double));
	memset(eval,0,nevMax*sizeof(double));
	ops->MultiVecCreateByMat(&evec,nevMax,A,ops);
	ops->MultiVecSetRandomValue(evec,0,nevMax,ops);
	void **gcg_mv_ws[4]; double *dbl_ws; int *int_ws;
	ops->MultiVecCreateByMat(&gcg_mv_ws[0],nevMax+2*block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[0],0,nevMax+2*block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[1],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[1],0,block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[2],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[2],0,block_size,ops);
	ops->MultiVecCreateByMat(&gcg_mv_ws[3],block_size,A,ops);				
	ops->MultiVecSetRandomValue(gcg_mv_ws[3],0,block_size,ops);
	int sizeV = nevInit + 2*block_size;
	int length_dbl_ws = 2*sizeV*sizeV+10*sizeV
		+(nevMax+2*block_size)+(nevMax)*block_size;
	// ops->Printf ( "length_dbl_ws = %d\n", length_dbl_ws );
	int length_int_ws = 6*sizeV+2*(block_size+3);
	// ops->Printf ( "length_int_ws = %d\n", length_int_ws );
	dbl_ws = malloc(length_dbl_ws*sizeof(double));
	memset(dbl_ws,0,length_dbl_ws*sizeof(double));
	int_ws = malloc(length_int_ws*sizeof(int));
	memset(int_ws,0,length_int_ws*sizeof(int));

	srand(0);
	double time_start, time_interval;
	time_start = ops->GetWtime();
		
	ops->Printf("===============================================\n");
	ops->Printf("GCG Eigen Solver\n");
	EigenSolverSetup_GCG(multiMax,gapMin,nevInit,nevMax,block_size,
		tol_gcg,max_iter_gcg,flag,gcg_mv_ws,dbl_ws,int_ws,ops);
	
	int    check_conv_max_num    = 50   ;
		
	char   initX_orth_method[8]  = "mgs"; 
	int    initX_orth_block_size = 80   ; 
	int    initX_orth_max_reorth = 2    ; double initX_orth_zero_tol    = 2*DBL_EPSILON;//1e-12
	
	char   compP_orth_method[8]  = "mgs"; 
	int    compP_orth_block_size = -1   ; 
	int    compP_orth_max_reorth = 2    ; double compP_orth_zero_tol    = 2*DBL_EPSILON;//1e-12
	
	char   compW_orth_method[8]  = "mgs";
	int    compW_orth_block_size = 80   ; 	
	int    compW_orth_max_reorth = 2    ;  double compW_orth_zero_tol   = 2*DBL_EPSILON;//1e-12
	int    compW_bpcg_max_iter   = 500   ;  double compW_bpcg_rate       = 1e-2; 
	double compW_bpcg_tol        = 1e-14;  char   compW_bpcg_tol_type[8] = "abs";
	
	int    compRR_min_num        = -1   ;  double compRR_min_gap        = gapMin;
	double compRR_tol            = 2*DBL_EPSILON;
		
	EigenSolverSetParameters_GCG(
			check_conv_max_num   ,
			initX_orth_method    , initX_orth_block_size, 
			initX_orth_max_reorth, initX_orth_zero_tol  ,
			compP_orth_method    , compP_orth_block_size, 
			compP_orth_max_reorth, compP_orth_zero_tol  ,
			compW_orth_method    , compW_orth_block_size, 
			compW_orth_max_reorth, compW_orth_zero_tol  ,
			compW_bpcg_max_iter  , compW_bpcg_rate      , 
			compW_bpcg_tol       , compW_bpcg_tol_type  , 0, // without shift
			compRR_min_num       , compRR_min_gap       ,
			compRR_tol           , compW_method         ,
			ops);		

	EigenSolverSetParametersFromCommandLine_GCG(argc,argv,ops);
	ops->Printf("nevGiven = %d, nevConv = %d, nevMax = %d, block_size = %d, nevInit = %d\n",
			nevGiven,nevConv,nevMax,block_size,nevInit);

	// struct GCGSolver_ *gcgsolver;
    // gcgsolver = (GCGSolver*)ops->eigen_solver_workspace;
	// gcgsolver->tol[0] = 1e+3;
	// gcgsolver->tol[1] = 1e-2;
	// int initnev = nevConv;
	// ops->EigenSolver(A,B,eval,evec,nevGiven,&initnev,ops);
	// gcgsolver->tol[0] = tol_gcg[0];
	// gcgsolver->tol[1] = tol_gcg[1];
	ops->EigenSolver(A,B,eval,evec,nevGiven,&nevConv,ops);
	ops->Printf("numIter = %d, nevConv = %d\n",
			((GCGSolver*)ops->eigen_solver_workspace)->numIter, nevConv);
	ops->Printf("++++++++++++++++++++++++++++++++++++++++++++++\n");

	time_interval = ops->GetWtime() - time_start;
	ops->Printf("Time is %.3f\n", time_interval);

	ops->MultiVecDestroy(&gcg_mv_ws[0],nevMax+2*block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[1],block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[2],block_size,ops);
	ops->MultiVecDestroy(&gcg_mv_ws[3],block_size,ops);
	free(dbl_ws); free(int_ws);

#if 1
	ops->Printf("eigenvalues\n");
	int idx;
	for (idx = 0; idx < nevConv; ++idx) {
		eval[idx] -= shift;
		ops->Printf("%.10f\n",eval[idx]);
	}
#endif
	ops->MultiVecDestroy(&(evec),nevMax,ops);
	free(eval);
	return 0;
}

int TestAppSLEPC(int argc, char *argv[]) 
{
	
	SlepcInitialize(&argc,&argv,(char*)0,help);
	PetscMPIInt   rank, size;
  	MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  	MPI_Comm_size(PETSC_COMM_WORLD, &size);	
	
	OPS *slepc_ops = NULL;
	OPS_Create (&slepc_ops);
	OPS_SLEPC_Set (slepc_ops);
	OPS_Setup (slepc_ops);
	slepc_ops->Printf("%s", help);
	slepc_ops->Printf("================================================\n");
	slepc_ops->Printf("===========(0)Load Mat K and M===========\n");
	
	void *matA, *matB; OPS *ops;
	double t0, t1;

	t0 = MPI_Wtime();

   	Mat      slepc_matA, slepc_matB;
   	PetscBool flg;
	PetscInt nrows, ncols;
#if 0
	PetscViewer    viewer;
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"bigK.petsc.bin",FILE_MODE_READ,&viewer); 
	MatCreate(PETSC_COMM_WORLD,&slepc_matA); 
	MatSetFromOptions(slepc_matA);
	MatLoad(slepc_matA,viewer); 
	PetscViewerDestroy(&viewer);
	MatGetSize(slepc_matA,&nrows,&ncols);
	slepc_ops->Printf("matrix A %d, %d\n",nrows,ncols);
	
	PetscViewer    viewer1;
	PetscViewerBinaryOpen(PETSC_COMM_WORLD,"bigM.petsc.bin",FILE_MODE_READ,&viewer1); 
	MatCreate(PETSC_COMM_WORLD,&slepc_matB); 
	MatSetFromOptions(slepc_matB);
	MatLoad(slepc_matB,viewer1); 
	PetscViewerDestroy(&viewer1);
	MatGetSize(slepc_matB,&nrows,&ncols);
	slepc_ops->Printf("matrix B %d, %d\n",nrows,ncols);
#else
	char file_matK[PETSC_MAX_PATH_LEN];
	PetscOptionsGetString(NULL,NULL,"-file_matK",file_matK,sizeof(file_matK),&flg);
	if (!flg) SETERRQ(PETSC_COMM_WORLD,1,"Must indicate a file name with the -file_matK option");
	slepc_ops->Printf("matK file : %s\n",file_matK);

	PetscViewer fd1, fd2;
	PetscViewerBinaryOpen(PETSC_COMM_WORLD, file_matK, FILE_MODE_READ, &fd1);
	MatCreate(PETSC_COMM_WORLD, &slepc_matA); 
	MatSetFromOptions(slepc_matA);
	MatLoad(slepc_matA,fd1); 
	PetscViewerDestroy(&fd1);
	MatGetSize(slepc_matA,&nrows,&ncols);
	slepc_ops->Printf("size of matrix K : %d, %d\n",nrows,ncols);

	char file_matM[PETSC_MAX_PATH_LEN];
	PetscOptionsGetString(NULL,NULL,"-file_matM",file_matM,sizeof(file_matM),&flg);
	if (!flg) SETERRQ(PETSC_COMM_WORLD,1,"Must indicate a file name with the -file_matM option");
	slepc_ops->Printf("matM file : %s\n",file_matM);

	PetscViewerBinaryOpen(PETSC_COMM_WORLD, file_matM, FILE_MODE_READ, &fd2);
	MatCreate(PETSC_COMM_WORLD, &slepc_matB); 
	MatSetFromOptions(slepc_matA);
	MatLoad(slepc_matB,fd2); 
	PetscViewerDestroy(&fd2);
	MatGetSize(slepc_matB,&nrows,&ncols);
	slepc_ops->Printf("size of matrix M : %d, %d\n",nrows,ncols);

#endif
	t1 = MPI_Wtime();
	slepc_ops->Printf("Time of Loading K and M   : %.2f s\n", t1 - t0);
	slepc_ops->Printf("===========(1)pre-treat Mat K and M===========\n");

	t0 = MPI_Wtime();

    Vec x, min, max;
    PetscInt idx;
    PetscReal max_num, min_num;
    MatCreateVecs(slepc_matA, &x, NULL);
    VecDuplicate(x, &max);
    VecDuplicate(x, &min);
    MatGetRowMaxAbs(slepc_matA, max, NULL);
    MatGetRowMinAbs(slepc_matA, min, NULL);
    VecMax(max, &idx, &max_num);
    VecMin(min, &idx, &min_num);
    slepc_ops->Printf("observe the elements of the given matrix\n");
    slepc_ops->Printf("max = %4.e, min = %4.e\n", max_num, min_num);
    VecDestroy(&max);
    VecDestroy(&min);
    VecDestroy(&x);
#if 1
	Vec diagK;
	Mat D_bigK;
	MatCreate(PETSC_COMM_WORLD, &D_bigK);
	MatSetSizes(D_bigK, PETSC_DECIDE, PETSC_DECIDE, nrows, ncols);
	MatSetUp(D_bigK);

	//construct D = diag(1./sqrt(diag(K))
	VecCreate(PETSC_COMM_WORLD, &diagK);
	VecSetSizes(diagK, PETSC_DECIDE, nrows);
	VecSetFromOptions(diagK);
	MatGetDiagonal(slepc_matA, diagK);
	VecSqrtAbs(diagK);
	VecPow(diagK, -1);
	
	MatDiagonalSet(D_bigK, diagK, INSERT_VALUES);
	MatAssemblyBegin(D_bigK, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(D_bigK, MAT_FINAL_ASSEMBLY);


	MatMatMatMult(D_bigK, slepc_matA, D_bigK,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&slepc_matA);
	MatMatMatMult(D_bigK, slepc_matB, D_bigK,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&slepc_matB);
	VecDestroy(&diagK);
	MatDestroy(&D_bigK);
#endif

	MatAssemblyBegin(slepc_matB, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(slepc_matB, MAT_FINAL_ASSEMBLY);

#if 1
    double sig = 1e+05;
	// MatAXPY(mpi_A,sig,mpi_B,DIFFERENT_NONZERO_PATTERN);
	MatAXPY(slepc_matA,sig,slepc_matB,DIFFERENT_NONZERO_PATTERN);
#endif
	MatAssemblyBegin(slepc_matA, MAT_FINAL_ASSEMBLY);
	MatAssemblyEnd(slepc_matA, MAT_FINAL_ASSEMBLY);

	t1 = MPI_Wtime();
	slepc_ops->Printf("Time of Pre-treat K and M : %.2f s\n", t1 - t0);
	slepc_ops->Printf("Shift                     : %f\n",sig);
	slepc_ops->Printf("================================================\n");
    //MatScale(mpi_A, 1e-9);
    // MatScale(mpi_B, 1e-9);

	int row_start, row_end, i;
	PetscInt           nc;
	const PetscInt    *aj;
	const PetscScalar *aa;

	ops = slepc_ops; 
	matA = (void*)(slepc_matA); matB = (void*)(slepc_matB);

	PetscInt use_slepc_eps = 0;
	PetscOptionsGetInt(NULL,NULL,"-use_slepc_eps",&use_slepc_eps,&flg);
	if (use_slepc_eps)
	{

	}
		// TestEPS(matA,matB,use_slepc_eps,argc,argv,ops);
	else {
		int flag = 0;
		TestEigenSolverGCG(matA,matB,flag,argc,argv,ops,sig);
	}
		
	/* 销毁petsc矩阵 */
   	MatDestroy(&slepc_matA);
   	MatDestroy(&slepc_matB);
	OPS_Destroy (&slepc_ops);
	SlepcFinalize();
	
	return 0;
}
#endif
int main(int argc, char *argv[]) 
{
	TestAppSLEPC(argc, argv);
   	return 0;
}




