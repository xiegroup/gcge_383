## 需要的外部环境

###  PETSc

需要用到直接解法器，在配置时需要同时安装一些外部包

```c
--download-fblaslapack=1 --download-scalapack  \
--download-metis --download-parmetis -–download-ptscotc \
--download-mumps --download-cmake --download-superlu --download-superlu_dist
```

再根据机器的实际环境安装即可

### SLEPc

在PETSc的基础上进行配置安装即可

> 在配置安装好PETSc和SLPEc后需要在设置环境变量(bashrc)

## 安装GCGE步骤

该版本通过链接GCGE的动态链接库来调用解法器，首先生成动态链接库：

在 GCGE 目录下执行

```bash
cmake .
make
```

执行完上述命令后，会得到 GCGE 的动态链接库(/GCGE/shared/lib)，相应的头文件在 /GCGE/shared/include。当然也可以直接执行脚本"docmake.sh" ，如果需要重新cmake可执行脚本 "recmake.sh":
```bash
sh recmake.sh
sh docmake.sh
```

/GCGE_v1.1/test_app_slepc.c 即为调用GCGE解法器的测试文件，在Makefile里需要指定GCGE的头文件地址和库的地址并链接动态库：

```makefile
-I${GCGEHOME}/include -L${GCGEHOME}/shared/lib -Wl,-rpath,${GCGEHOME}/shared/lib -lGCGE
```

其中GCGEHOME为 GCGE_v1.1的路径。在编译之前修改/GCGE/Makefile的GCGEHOME为当前GCGE的安装路径，如果有需要请根据实际情况修改make.MPI.inc。目前提供的/GCGE_v1.1/make.MPI.inc指定了PETSc和SLEPc的相关编译选项，由于在安装PETSc时指定了不同于源码包路径的安装路径，PETSC_ARCH为空，如果用户PETSc的安装路径和源码包路径一致，需要修改PETSCINC,LIBPETSC,SLEPCINC,LIBSLEPC:

```makefile
PETSCINC   =  -I${PETSC_DIR}/include -I${PETSC_DIR}/${PETSC_ARCH}/include -I${PETSC_DIR}/include/petsc/private  
LIBPETSC   = -L${PETSC_DIR}/${PETSC_ARCH}/lib -Wl,-rpath,${PETSC_DIR}/${PETSC_ARCH}/lib -lpetsc \
SLEPCINC   = -I${SLEPC_DIR}/include -I${PETSC_DIR}/include -I${SLEPC_DIR}/${PETSC_ARCH}/include  \
LIBSLEPC   = -Wl,-rpath,${SLEPC_DIR}/${PETSC_ARCH}/lib -L${SLEPC_DIR}/${PETSC_ARCH}/lib -lslepc -Wl,-rpath,${PETSC_DIR}/${PETSC_ARCH}/lib -L${PETSC_DIR}/${PETSC_ARCH}/lib  
```
修改完后，执行
```makefile
make
```
即可得到可执行文件
```makefile
TestGCGE.exe
```
运行示例
```makefile
mpirun -np 288 ./TestGCGE.exe -file_matK K.dat -file_matM M.dat
```
参数说明，参看/GCGE_v1.1/test_app_slepc.c, Line 19-25
```c
	int nevConv  = 10; //number of required eigenpairs 
	double gapMin = 1e-5, abs_tol, rel_tol;
	int nevGiven = 0, block_size = nevConv/2, nevMax = 2*nevConv, multiMax = 1, nevInit = nevMax;
	abs_tol = 1e-7; //abs tolerance
	rel_tol = 1e-9; //rel tolerance
	//to choose the method for linear solver
	char compW_method[8] = "bcg"; //block cg solver in computing w, or you can choose 'mumps'
```