# GCGE_v1.1安装及调用说明

## 求解器简介

GCGE_v1.1 是基于 PETSc 矩阵向量结构设计的，软件包结构如下

![LAPACK](https://raw.githubusercontent.com/WangZijingxiannv/GCGE_HTML/main/software.jpg)

- Level1: 矩阵-多向量操作，相应的源程序文件在文件夹 /GCGE/app/

  > app_slepc.c    基于SLEPc矩阵向量结构实现的基本操作
  >
  > app_lapack.c  基于稠密矩阵向量结构的基本操作

- Level2: 块正交化(MGS)和线性解法器(BCGS)，相应的源程序文件在文件夹 /GCGE/src

  > ops_lin_sol.c 求解线性方程组，block conjugate gradient method
  >
  > ops_orth.c     正交化，block modified Gram-Schmidt（default）

- Level3: GCG算法，相应的源程序文件在文件夹 /GCGE/src

  > ops_eig_sol_gcg.c 算法主体见GCG()

## 需要的外部环境

###  PETSc

需要用到直接解法器，在配置时需要同时安装一些外部包

```c
--download-fblaslapack=1 --download-scalapack  \
--download-metis --download-parmetis -–download-ptscotc \
--download-mumps --download-cmake --download-superlu --download-superlu_dist
```

再根据机器的实际环境安装即可

### SLEPc

在PETSc的基础上进行配置安装即可

> 在配置安装好PETSc和SLPEc后需要在设置环境变量(bashrc)
>

## 安装GCGE步骤

该版本通过链接GCGE的动态链接库来调用解法器，首先生成动态链接库：

在 GCGE 目录下执行

```bash
cmake .
make
```

执行完上述命令后，会得到 GCGE 的动态链接库(/GCGE/shared/lib)，相应的头文件在 /GCGE/shared/include。当然也可以直接执行脚本"docmake.sh" ，如果需要重新cmake可执行脚本 "recmake.sh"。

/GCGE_v1.1/test_app_slepc.c 即为调用GCGE解法器的测试文件，在Makefile里需要指定GCGE的头文件地址和库的地址并链接动态库：

```makefile
-I${GCGEHOME}/include -L${GCGEHOME}/shared/lib -Wl,-rpath,${GCGEHOME}/shared/lib -lGCGE
```

其中GCGEHOME为 GCGE_v1.1的路径。



## 3.8.3 changelog

### 配置选项

根据要求，目前 PETSc 和 SLPEc 的版本都已经按照版本3.8.3配置安装。PETSc的配置选项如下

```bash
   --download-metis --download-parmetis -–download-ptscotch \
   --download-mumps --download-cmake --download-superlu --download-superlu_dist \
    --with-debugging=0 \
   COPTFLAGS='-O2 -march=native -mtune=native -mkl=cluster -lifcore' \
   CXXOPTFLAGS='-O2 -march=native -mtune=native -mkl=cluster -lifcore' \
   FOPTFLAGS='-O2 -march=native -mtune=native -mkl=cluster' \
   --with-scalapack=1 
```

SLEPc 的配置未加选项

### 跟最新版本相比，涉及到以下需要修改的地方

#### Slepc

#### Version3.17.2

```c
 //说明：3.8.3版本中还没实现以下函数功能 
BVMatMultTranspose
  //y = A^T * x
  // change : BVMatMultHermitianTranspose（3.8.3）
BVGetMat
  //BV to dense mat
  //change : BVGetColumn（3.8.3）
BVRestoreMat
  //restore mat from bv
 //change : BVRestoreColumn
PCFactorSetMatSolverType
  //预条件解法器选项 
  //chenge：PCFactorSetMatSolverPackage PCFactorSetUpMatSolverPackage（3.8.3）
```

#### Petsc

#### Version3.17.2

```c
 //说明：3.8.3版本中还没实现以下函数功能 
MatDenseGetArrayRead--MatDenseGetArray
   //change : MatDenseGetArray（3.8.3）
MatDenseRestoreArrayRead--MatDenseRestoreArray
   //change : MatDenseRestoreArray（3.8.3）
KSPMatSolve--KSPSolve
  //change : KSPSolve
```

> PCFactorSetMatSolverType和KSPSolve是调用直接法求解线性方程组时用到的函数
>
> kspmatsolve可以看成是kspsolve的块形式，如用直接法来实现GCGE算法中计算W步骤，在3.8.3版本下，必须一列一列去计算W

### 矩阵预处理部分

三个矩阵相乘

Version3.17.2

```c
MatProductSetType
MatProductSymbolic
MatProductNumeric
  //change: MatMatMatMult(3.8.3)
```



## 运行说明及其他

算例1: 矩阵规模330,048 

算例2: 矩阵规模 17,723,241 

两个算例的刚度矩阵 $K$ 的最大和最小元素的量级差别都很大，质量矩阵 $M$ 都几乎对角。例1对称正定，算例2应该有一个零特征对。

### 矩阵

输入矩阵 $K$ 和 $M$ ，输入方式：在运行程序时使用命令行选项，例如

```bash
-file_matK K.dat -file_matM M.dat
```

矩阵读入代码见 /GCGEv1.1/test_app_slepc.c。

- 读入后矩阵格式为 mpiaij 或 seqaij，由使用的进程个个数决定
- 由于矩阵较为病态，矩阵最大最小元素量级差别极大，需要先对矩阵进行预处理将问题进行等价转换
- 考虑到矩阵规模较大的情形，矩阵的预处理步骤在GCGE里通过调用PETSc的一些操作来完成，经测试这部分时间占比较小
- 根据问题的特点，先对矩阵 $K$ 做一个位移 $\theta$ : $K \leftarrow K + \theta M$，这里 $\theta = 100,000$(经验参数)，可以加速收敛

> 当然也可以不用命令行，直接在程序里直接读入矩阵
>
> 如果矩阵可以提前处理，比如用petsc里与matlab交互矩阵格式的接口，可以将矩阵存为petsc.bin的格式。因为在两个测试算例里矩阵 M 都是几乎对角的，如果先进行这样的转换可以减少 M 的存储空间

### 线性解法器选择说明(计算W)

结合问题的性质，可以用直接法来实现GCG算法中的计算 $W$ 步骤以提高该步骤的精度，从而减少整体的迭代步骤，加速GCG算法的收敛。但是使用 MUMPS 求解对计算环境有一定的要求。

- MUMPS

  在矩阵规模较大时(阈值跟机器有关)，比如第二个测试算例，矩阵规模17723241，非零元个数1,303,211,137。如果直接调用 MUMPS 作为直接解法器会出现内存错误。通过查阅相关资料和测试，我们在LSSC4(http://lsec.cc.ac.cn/chinese/lsec/LSSC-IVintroduction.pdf)计算环境下，每个计算节点使用 8 个进程，使用不少于 8 个计算节点，调用MUMPS可以成功计算线性方程组。

- SUPERLU_DIST

  第一个算例可以使用SUPERLU_DIST，第二个算例由于非零元素个数过多，需要修改 PETSc 的相关配置才能使用(未测试)

当然也可以直接使用迭代法来执行计算W这一步，修改/GCGEv1.1/test_app_slepc.c 里的参数

```c
compW_method
```

再重新cmake即可

```c
char compW_method[8] = "bcg"; //or mumps
```

## 测试报告

精度要求 : 相对1e-9 绝对1e-7 （可以更高）

因为特征值较大，GCGE在迭代过程中要求两个精度同时满足，因此实际的相对精度能达到1e-11

|  矩阵规模  | Solver | 求解特征对个数 | 计算W步骤的方法 | 进程个数 |  CPU time(LU set)  | Iteration | 备注     |
| :--------: | :----: | :------------: | :-------------: | :------: | :----------------: | :-------: | -------- |
|  330,048   |  GCGE  |       10       |      MUMPS      |    72    |   7.76(==3.75==)   |    14     | 2个节点  |
|  330,048   |  GCGE  |       10       |       BCG       |    72    |       72.50        |    167    | 2个节点  |
| 17,723,241 |  GCGE  |       10       |      MUMPS      |   128    | 830.92(==581.41==) |    15     | 8个节点  |
| 17,723,241 |  GCGE  |       10       |    BCG(100)     |   288    |    ==6088.06==     | ==1023==  | 8个节点  |
| 17,723,241 |  GCGE  |       10       |    BCG(500)     |   288    |    ==3525.345==    |  ==169==  | 36个节点 |
| 17,723,241 |  GCGE  |       10       |    BCG(600)     |   288    |                    |           | 8个节点  |

前10个特征对

- 330,048 

  ```c
  92.3503616553
  616.8714727082
  4362.4293694253
  4412.8646079079
  12283.0169542413
  18427.7842039714
  24326.7426415578
  57631.4241941953
  59042.5611721122
  107249.1365763134
  ```

  

- 17,723,241（应该有零特征值）

  ```c
  0.0000113890
  303.6027458086
  1900.6309172987
  4149.6488663827
  6212.9225057423
  9023.2670596233
  16455.1775764513
  28218.8941120560
  41727.7776244366
  64072.8658948287
  ```

  



> 330,048   : 13,283MB (blockCG)
>
> 17,723,241: 1,150,959MB (MUMPS)











